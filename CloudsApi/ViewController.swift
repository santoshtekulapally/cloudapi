//
//  ViewController.swift
//  CloudsApi
//
//  Created by santosh tekulapally on 2019-11-04.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import WatchConnectivity
import Particle_SDK


class ViewController: UIViewController, WCSessionDelegate {

    
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        
        
        print("wow phone revieced")
        
    }
    
    func session(_ session: WCSession,
                 didFinish userInfoTransfer: WCSessionUserInfoTransfer,
                 error: Error?){
        
        print("wow phone revieced")
        
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("Phone: I received a message:")
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Phone: I received a message:")
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Phone: I received a message:")
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        
        
        DispatchQueue.main.async {
            
            
            
            
            print("Phone: I received a message:")
            
            
            print("Phone: I received a message: \(message)")
            
            // Get the "name" key out of the dictionary
            // and show it in the label
            self.latitude = message["lat"] as! String
            self.longitude = message["lon"] as! String
            self.isfromwatch = true
            //let id = message["id"] as! String
            self.hitserver()

          //  self.messagefromwatch.text = ("Name" + "   " + Firstname + "   "  + "age" + "   " + age )
            
            //
            //        message["name"] ---> Pritesh
            //
            //
            //        var person = {"name":"pritesh", "age":"25", "id":"C02343"}
            //        console.log(person["name"]) ---> "Pritesh"
            //        console.log(person["id"]) ---> "C02343"
            //
            
        }
    }
    
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var windsppedlbl: UILabel!
    @IBOutlet weak var pressurelbl: UILabel!
    @IBOutlet weak var humidlbl: UILabel!
    @IBOutlet weak var templbl: UILabel!
    @IBOutlet weak var weatherlbl: UILabel!
    @IBOutlet weak var citylabel: UILabel!
    
    var cityCoordinates:CLLocationCoordinate2D?

    var isfromwatch = false
    var displaytime:String!
    var city:String!
    var latitude:String!
    var longitude:String!
    
    
    
    let USERNAME = "santoshonthemove@gmail.com"
    let PASSWORD = "santosh@123"
    
    // MARK: Device
    let DEVICE_ID = "25001a000f47363333343437"
    var myPhoton : ParticleDevice?
    
    
    
    
    
    override func viewDidLoad()
    {
        
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        
        
        
        
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                self.getDeviceFromCloud()
            }
        }
        
        isfromwatch = false

        
        if (WCSession.isSupported() == true) {
           // messagefromwatch.text = "WC is supported!"
            print("WC supported!")
            // create a communication session with the watch
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else {
            
             print("WC NOT supported!")
           // messagefromwatch.text = "WC NOT supported!"
        }
        
        

        super.viewDidLoad()
        //let preferences = UserDefaults.standard
        
       self.hitserver()

        // Do any additional setup after loading the view.
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            
            
            
            
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon: \(device?.id)")
                self.myPhoton = device
            }
            
        } // end getDevice()
    }
    func hitserver() {
        
        
        
        var dateformatter : DateFormatter
        
        var lati = ""
        var longi = ""
        var  city = ""
        
        if (isfromwatch == true){
            
            lati = latitude
            longi  = longitude
        }
        else if (isfromwatch == false)
        {
                lati = "45.4972159"
                   longi = "-73.610364200"
                  city = "Toronto"
        }
        //self.cityLabel.setText(city)
        
        //  self.weatherlabel.setText("hfdchgcvh")
        
        //      //  print("SHARED PREFERENCES OUTPUT")
        //        print(preferences.string(forKey: "savedLat"))
        //        print(preferences.string(forKey: "savedLng"))
        //        print(preferences.string(forKey: "savedCity"))
        //
        //
//        var lat = ""
//        var lng = ""
//        var city = ""
//
//        //        var lat = preferences.string(forKey:"savedLat")
//        //        var lng = preferences.string(forKey:"savedLng")
//        //        var city = preferences.string(forKey:"savedCity")
//
//        //if (lat == nil || lng == nil || city == nil) {
//        latitude = "45.4972159"
//        longitude = "-73.610364200"
//        city = "Toronto"
//
        // }
        
        let URL = "https://api.darksky.net/forecast/e84691c4c8418637c327a9551bb47d94/\(lati),\(longi)"
        
        
        // let URL = "https://api.sunrise-sunset.org/json?lat=\(lat!)&lng=\(lng!)&date=today"
        print("Url: \(URL)")
        
        //  AF.request(URL).responseJSON
        
        // Alamofire.request(URL).responseJSON
        AF.request(URL, method: .get, parameters:nil).responseJSON {(response) in
            
            //
            //        if(response.result.isSuccess){
            //            print("I got something")
            //            print("I got something")
            //            print("I got something")
            //            print("I got something")
            //            print("I got something")
            //           // print(responseJSON.data)
            //
            //
            
            do{
                
                
                let json = try JSON(data: response.data!)
                print(json)
                print("latitude \(json["latitude"])")
                print("longitude \(json["longitude"])")
                
                print("time is   \(json["currently"]["time"])")
                print("weather is   \(json["currently"]["icon"])")
                
                print("humidity is \(json["currently"]["humidity"])")
                print("chance of rain \(json["currently"]["precipProbability"])%")
                
                print("temparature is  \(json["currently"]["temperature"])")
                print("pressure is  \(json["currently"]["pressure"])")
                
                //  var timme: String!
                //  let timme = json["currently"]["time"]
                // self.weatherlabel.setText("\(json["currently"]["icon"])")
                self.templbl.text = ("\(json["currently"]["temperature"])")
                self.humidlbl.text = ("\(json["currently"]["humidity"])")
                self.pressurelbl.text = ("\(json["currently"]["pressure"])")
                self.windsppedlbl.text = ("\(json["currently"]["windSpeed"]) km/h")
                self.weatherlbl.text = ("\(json["currently"]["summary"])")
                self.citylabel.text = ("\(json["timezone"])")
                
                
                //                let weather = json["currently"]["time"].string
                //                let humidity = json["results"]["sunrise"].string
                //                let rainchance = json["results"]["sunrise"].string
                //                let temparature = json["results"]["sunrise"].string
                //                let atmospressure = json["results"]["sunrise"].string
                
                let dateee = "\(json["currently"]["time"])"
                // let sunsetTime = jsonResponse["results"]["sunset"].string
                print( "print ate is \(dateee)")
                
                
                
                let unixTimestamp = Double(dateee)
                
                let date = Date(timeIntervalSince1970: unixTimestamp!)
                
             
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                let strDate = dateFormatter.string(from: date)

                dateFormatter.dateFormat = "HH"
                //Specify your format that you want
                self.displaytime = dateFormatter.string(from: date)


                print( "print ate is \(strDate)")
                
                if(self.isfromwatch == true){
                   
                    print( "print ate is \(self.displaytime)")
                    
                   // UserDefaults.standard.set(self.displaytime, forKey: "timeshow")
                    
                    self.showtimeonparticle()
                    
                    
                }else{
                   
                }
                
                
                self.timelbl.text = (strDate)
                
                //
                //                self.humidty.text = "\(json["currently"]["humidity"])%"
                //
                //                self.temparature.text = "\(json["currently"]["temperature"])"
                //
                //                //  self.date.text = json["currently"]["humidity"].string
                //
                //                self.rain.text = "\(json["currently"]["precipProbability"])%"
                //
                //                self.windspeed.text = "\(json["currently"]["precipProbability"]) km/h"
                //
                //self.buttontime.text = date
                
                
                
                
            }
            catch{
                
            }
            
            //        }
            //        else{
            //
            //            print("error error")
            //
        }
        
        
    }
    
     func showtimeonparticle() {
        
        let parameters = [String(self.displaytime)]
        
        var task = myPhoton!.callFunction("time", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to show score: \(self.displaytime)")
            }
            else {
                print("Error when telling Particle to show score")
            }
        }
        
        
    }
   
    
    func geocode(cityName:String) {
        let geocoder = CLGeocoder()
        print("Trying to geocode \(cityName) to lat/lng")
        geocoder.geocodeAddressString(cityName) {
            
            (placemarks, error) in
            
            print("Got a response")
            // Process Response
            if let error = error {
                print("Unable to Forward Geocode Address (\(error))")
                self.citylabel.text = "Unable to Find Location for Address"
                self.cityCoordinates = nil
            } else {
                var location: CLLocation?
                
                if let placemarks = placemarks, placemarks.count > 0 {
                    location = placemarks.first?.location
                    self.cityCoordinates = location?.coordinate
                }
                
                if let location = location {
                    self.cityCoordinates = location.coordinate
                    
                } else {
                    print("No matching location found")
                    self.citylabel.text = "No Matching Location Found"
                    self.cityCoordinates = nil
                }
            }
            
            print("City coordinates: \(self.cityCoordinates)")
        }
    }

}

