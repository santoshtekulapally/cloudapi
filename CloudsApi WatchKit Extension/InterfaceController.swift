//
//  InterfaceController.swift
//  CloudsApi WatchKit Extension
//
//  Created by santosh tekulapally on 2019-11-04.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet weak var loadingsunriceimg: WKInterfaceImage!
    
    @IBOutlet weak var timelabel: WKInterfaceLabel!
    
    @IBOutlet weak var loadingsunsetimg: WKInterfaceImage!
    
  
    
    
    
     @IBOutlet var wallkit:  WKInterfaceLabel!
    
    //@IBOutlet var weathernow: WKInterfaceLabel!
    @IBOutlet var citylabell: WKInterfaceLabel!
    
    @IBOutlet var Temparataure: WKInterfaceLabel!
    @IBOutlet var Humidity: WKInterfaceLabel!
    @IBOutlet var pressure: WKInterfaceLabel!
    @IBOutlet var Windspeed: WKInterfaceLabel!
    
    var cityCoordinates:CLLocationCoordinate2D?

    
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        print("sent message to phone amnd phone recieved it")
        
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        print("WATCH: I received a message: \(message)")
        
        // Get the "name" key out of the dictionary
        // and show it in the label
        let Firstname = message["name"] as! String
        let firstcolour = message["colour"] as! String
        //let id = message["id"] as! String
      //  messagefromphonelbl.setText("Name " + Firstname + "colour" + firstcolour)
        
        
        //CatchString = Firstname
        //
        //        message["name"] ---> Pritesh
        //
        //
        //        var person = {"name":"pritesh", "age":"25", "id":"C02343"}
        //        console.log(person["name"]) ---> "Pritesh"
        //        console.log(person["id"]) ---> "C02343"
        //
        
        
    }
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if (WCSession.isSupported() == true) {
            
           print("WC is supported!")
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WC NOT supported!")
        }
        
        
        
        let preferences = UserDefaults.standard
        
         var dateformatter : DateFormatter
        //self.cityLabel.setText(city)

      //  self.weatherlabel.setText("hfdchgcvh")

        print("SHARED PREFERENCES OUTPUT")
        print(preferences.string(forKey: "savedLat"))
        print(preferences.string(forKey: "savedLng"))
        print(preferences.string(forKey: "savedCity"))
        
        
        var lat = preferences.string(forKey:"savedLat")
        var lng = preferences.string(forKey:"savedLng")
        var city = preferences.string(forKey:"savedCity")
        
        if (lat == nil || lng == nil || city == nil) {
            lat = "43.667780"
            lng = "-79.337380"
            city = "Vancouver"
            
        }
        
        // Update UI
       // self.cityLabel.setText(city)
        
        // start animations
       // self.showLoadingAnimations()
        
        // TODO: Put your API call here
        
        //api.darksky.net/forecast/e84691c4c8418637c327a9551bb47d94/\(lat!)&lng=\(lng!),255657600?exclude=currently,flags
        let URL = "https://api.darksky.net/forecast/e84691c4c8418637c327a9551bb47d94/\(lat!),\(lng!)"
        
        
       // let URL = "https://api.sunrise-sunset.org/json?lat=\(lat!)&lng=\(lng!)&date=today"
        print("Url: \(URL)")

      //  AF.request(URL).responseJSON
        
       // Alamofire.request(URL).responseJSON
        AF.request(URL, method: .get, parameters:nil).responseJSON {(response) in

//
//        if(response.result.isSuccess){
//            print("I got something")
//            print("I got something")
//            print("I got something")
//            print("I got something")
//            print("I got something")
//           // print(responseJSON.data)
//
//
            
            do{
                
                
                let json = try JSON(data: response.data!)
                print(json)
                print("latitude \(json["latitude"])")
                print("longitude \(json["longitude"])")
                
                print("time is   \(json["currently"]["time"])")
                print("weather is   \(json["currently"]["icon"])")

                print("winddd sspeeeeedde \(json["currently"]["windSpeed"])")
                print("chance of rain \(json["currently"]["precipProbability"])%")

                print("temparature is  \(json["currently"]["temperature"])")
                 print("pressure is  \(json["currently"]["pressure"])")

              //  var timme: String!
               //  let timme = json["currently"]["time"]
             // self.weatherlabel.setText("\(json["currently"]["icon"])")
                self.Temparataure.setText("\(json["currently"]["temperature"])")
                self.Humidity.setText("\(json["currently"]["humidity"])")
                self.pressure.setText("\(json["currently"]["pressure"])")
                self.Windspeed.setText("\(json["currently"]["windSpeed"])")
                self.wallkit.setText("\(json["currently"]["icon"])")
                self.citylabell.setText("\(json["timezone"])")

                print("the city is  \(json["timezone"])")
//                let weather = json["currently"]["time"].string
//                let humidity = json["results"]["sunrise"].string
//                let rainchance = json["results"]["sunrise"].string
//                let temparature = json["results"]["sunrise"].string
//                let atmospressure = json["results"]["sunrise"].string

                let dateee = "\(json["currently"]["time"])"
               // let sunsetTime = jsonResponse["results"]["sunset"].string
                print( "print ate is \(dateee)")
                
              
                
                let unixTimestamp = Double(dateee)
                
                let date = Date(timeIntervalSince1970: unixTimestamp!)
                
               
                
                
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
                let strDate = dateFormatter.string(from: date)
                
                 print( "print ate is \(strDate)")
                
                
                self.timelabel.setText(strDate)

                //
//                self.humidty.text = "\(json["currently"]["humidity"])%"
//
//                self.temparature.text = "\(json["currently"]["temperature"])"
//
//                //  self.date.text = json["currently"]["humidity"].string
//
//                self.rain.text = "\(json["currently"]["precipProbability"])%"
//
//                self.windspeed.text = "\(json["currently"]["precipProbability"]) km/h"
//
                //self.buttontime.text = date
                
                
                
                
            }
            catch{
                
            }
            
//        }
//        else{
//
//            print("error error")
//
        }
        
        
        
//            {
//
//
//
//
//
//            // 1. store the data from the internet in the
//            // response variable
//            response in
//
//            // 2. get the data out of the variable
//            guard let apiData = response.value.result else {
//                print("Error getting data from the URL")
//                return
//            }
            // GET sunrise/sunset time out of the JSON response
//            let jsonResponse = JSON(apiData)
//            let sunriseTime = jsonResponse["results"]["sunrise"].string
//            let sunsetTime = jsonResponse["results"]["sunset"].string

            // display in a UI
//            self.sunriseLabel.setText("\(sunriseTime!)")
//            self.sunsetLabel.setText("\(sunsetTime!)")
//
//            // stop the loading animation
//            self.stopAnimations()
       // }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func changeCityButtonPressed() {
        
        
        
        print("score button pressed")
        
        
        //        let name = UserDefaults.standard.string(forKey: "score")
        //
        //        print(name)        // 1. Show the score in the Phone
        // ------------------------------
        // self.scoreLabel.text = "Score:\(self.gameScore)"
        
        // 2. Send score to Particle
        // ------------------------------
        //            let parameters = 3
        //            var task = myPhoton!.callFunction("score", withArguments: parameters) {
        //                (resultCode : NSNumber?, error : Error?) -> Void in
        //                if (error == nil) {
        //                    print("Sent message to Particle to show score: \(self.gameScore)")
        //                }
        //                else {
        //                    print("Error when telling Particle to show score")
        //                }
        //            }
        
        
        
        // 3. done!
        
        
    }
    
    
    
    func showLoadingAnimations() {
        // animate the sunrise label
      //  self.loadingsunriceimg.setImageNamed("Progress")
//        self.loadingSunriseImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
     //   self.weatherlabel.setText("Updating...")
        
        // animate the sunset label
      //  self.loadingsunsetimg.setImageNamed("Progress")
       // self.loadingSunsetImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
      //  self.weatherlabel.setText("Updating...")
    }
    
    func stopAnimations() {
        // sunrise loading image
       // self.loadingsunriceimg.stopAnimating()
       // self.weather.setImage(nil)
        
        // sunset loading image
      //  self.loadingsunsetimg.stopAnimating()
       // self.loadingSunsetImage.setImage(nil)
    }
    
}

