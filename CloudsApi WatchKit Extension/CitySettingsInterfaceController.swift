//
//  CitySettingsInterfaceController.swift
//  CloudsApi WatchKit Extension
//
//  Created by santosh tekulapally on 2019-11-05.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class CitySettingsInterfaceController: WKInterfaceController , WCSessionDelegate{
  
  
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        print("sent message to phone amnd phone recieved it")
        
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        print("WATCH: I received a message: \(message)")
        
        // Get the "name" key out of the dictionary
        // and show it in the label
        let Firstname = message["name"] as! String
        let firstcolour = message["colour"] as! String
        //let id = message["id"] as! String
        //  messagefromphonelbl.setText("Name " + Firstname + "colour" + firstcolour)
        
        
        //CatchString = Firstname
        //
        //        message["name"] ---> Pritesh
        //
        //
        //        var person = {"name":"pritesh", "age":"25", "id":"C02343"}
        //        console.log(person["name"]) ---> "Pritesh"
        //        console.log(person["id"]) ---> "C02343"
        //
        
        
    }

    
    
    // MARK: Outlets
    @IBOutlet var savedCityLabel: WKInterfaceLabel!
    @IBOutlet var loadingImage: WKInterfaceImage!
    @IBOutlet var saveButtonLabel: WKInterfaceLabel!
    
    // MARK: Variables
    var city:String!
    var latitude:String?
    var longitude:String?
    
    // MARK: API KEYS
    let API_KEY = "bb1312b9aea363"
    var mainVC:InterfaceController?
    
    @IBAction func selectCityPressed() {
        // 1. When person clicks on button, show them the input UI
        let suggestedResponses = ["Toronto", "Montreal","New York City","Los Angeles"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.savedCityLabel.setText(userResponse)
                self.city = userResponse
                
                
            }
        }
    }
    
    @IBAction func saveButtonPressed() {
        
//        if (self.city.isEmpty)
//        {
//            return
//        }
//        else{
        
        // Send the message
        print("Getting City")
        
        self.geocode(cityName: self.city)
      
       // }
    }
    
    
    func geocode(cityName:String) {
        
        // Get lat long using LocationIQ API (https://locationiq.com)
        // You can use Google, but you need to give Google your credit card number.
        // You can use the built in Apple CoreLocation library, but this library doesn't work properly with the emulator. See notes below.
        
        
        // TODO: Put your API call here
        
        // Encode the city name
        let cityParam = cityName.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        
        // start showing the loading image
        
        self.loadingImage.setImageNamed("Progress")
        self.loadingImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
        self.saveButtonLabel.setText("Saving...")
        
        
        let URL = "https://us1.locationiq.com/v1/search.php?key=\(self.API_KEY)&q=\(cityParam!)&format=json"
        print(URL)
        
        AF.request(URL, method: .get, parameters:nil).responseJSON {(response) in
            // 1. store the data from the internet in the
            // response variable
            
            
            
            
           
            do{
                
                let json = try JSON(data: response.data!)
                print(json)
//                let json = try JSON(data: response.data!)
//                print(json)
//                var jsonResponse = JSON(response)
                
//                print(jsonResponse)
                let results = json.array?.first
//
//                if (results == nil) {
//                    print("Error parsing results from JSON response")
//                    return
//                }
                
              //  let data = JSON(results)
//
//                self.latitude = data["lat"].string
//                self.longitude = data["lon"].string
               // let json = try JSON(data: response.data!)
              //  print("araaaayy isss \(results.["lat"])")
                print("lat iiss ss \(results!["lat"])")
                print("lat iiss ss \(results!["lon"])")


                self.latitude = "\(results!["lat"])"
                self.longitude = "\(results!["lon"])"
                
                
                let message = ["lat":self.latitude,
                               "lon":self.longitude] as [String : Any]
                
                WCSession.default.sendMessage(message, replyHandler:nil)
                
                
            }
            catch{
                
            }
            
         
            
            print("Lat: \(self.latitude)")
            print("Long: \(self.longitude)")
            
            
            // save this to Shared Preferences
            let preferences = UserDefaults.standard
            preferences.set(self.latitude, forKey:"savedLat")
            preferences.set(self.longitude, forKey:"savedLng")
            preferences.set(self.city, forKey:"savedCity")
            
            // dismiss the View Controller
            self.popToRootController()
            
            
            self.loadingImage.stopAnimating()
        }
        
        
        // MARK: Geocoding with CoreLocation libraries
        // -------------
        
        //Note:  This is the code for performing geolocation with the built in CoreLocation library.
        //But, the code doesn't work properly with the watchOS Emulator.
        //It WILL work with a real watch -- just not the simulator.
        //This code remains here for reference purposes
        
        /*
         let geocoder = CLGeocoder()
         
         geocoder.geocodeAddressString(cityName, completionHandler: {
         
         (placemarks, error) -> Void in
         
         if((error) != nil){
         print("Error", error)
         }
         if let placemark = placemarks?.first {
         let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
         print(coordinates)
         }
         })
         */
        
    }
    
    // MARK: Default functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if (WCSession.isSupported() == true) {
            
            print("WC is supported!")
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WC NOT supported!")
        }
        
        // get name of city in shared preferences
        let preferences = UserDefaults.standard
        guard let savedCity = preferences.string(forKey: "savedCity") else {
            return
        }
        
        self.savedCityLabel.setText(savedCity)
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}

